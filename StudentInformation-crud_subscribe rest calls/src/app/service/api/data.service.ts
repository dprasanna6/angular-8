import { Injectable } from '@angular/core';
import { getLocaleDateFormat } from '@angular/common';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DataService {

constructor(private http : HttpClient) {}
getData = (url)=>{  
 return this.http.get(url); 
}

postData = (url,postObj)=>{
   return this.http.post(url,postObj);
}
deleteData = (url) =>{
  return this.http.delete(url); 
}
updateData =(url,putObj) =>{
  return this.http.put(url,putObj); 
}

   }

