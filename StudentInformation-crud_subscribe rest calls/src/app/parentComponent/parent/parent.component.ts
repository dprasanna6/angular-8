import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/api/data.service';
import { environment } from 'src/environments/environment';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {  
  studentsInformation : Object = [];
  selectedStudentData : any = null;  
  baseUrl:string =`${environment.baseUrl}studentsInformation`;   
  constructor(private dataservice: DataService) {

   }
 /**
  * 
  * @description getData
  */
   getData = () =>{
    this.dataservice.getData(this.baseUrl).subscribe((response)=>{
      this.studentsInformation=response;
      console.log(this.studentsInformation);
    },(error)=>{
      console.log(error)
    },()=>{})  

   }


   /**
    * 
    * @description Delete data
    * 
    */
   onSelectDelete =(id) =>{
   console.log(id);
   this.dataservice.deleteData(`${this.baseUrl}/${id}`).subscribe((response)=>{    
    console.log(response);
    this.getData();
  },(error)=>{
    console.log(error)
  },()=>{})    
  }

/**
 * 
 * @description post user data
 * 
 */
  addUser = () =>{
    console.log("add User");
    let student={firstName: 'danda',lastName:'danda',studentId:'105',address:'hyderabad1',email:'dprasann6@gmail.com',mobile:Math.random}
    this.dataservice.postData(this.baseUrl,student).subscribe((response)=>{     
      console.log(response);
      this.getData();
    },(error)=>{
      console.log(error)
    },()=>{})  

  }

  
  /**
   * @description update user data
   * 
   */
  updateUser = () =>{
    // console.log("update User");
    // let student={firstName: 'HCL',lastName:'danda',studentId:'104',address:'hyderabad',email:'dprasann6@gmail.com',mobile:Math.random}
    // this.dataservice.postData(this.baseUrl,student).subscribe((response)=>{     
    //   console.log(response);
    //   this.getData();
    // },(error)=>{
    //   console.log(error)
    // },()=>{})  
    console.log(this.selectedStudentData);
    this.dataservice.updateData(`${this.baseUrl}/${this.selectedStudentData.id}`,this.selectedStudentData).subscribe((response)=>{     
      console.log(response);
      this.getData();
    },(error)=>{
      console.log(error)
    },()=>{}) 

  }
    
   ngOnInit(): void {
     this.getData();      
   }

  
  onSelectStudent =(student) =>{
    this.selectedStudentData=student;    
    console.log("In Parent",student);   
  };

   
  trackBystudentId = (index : number , student : any) => {
    return student.id;
  }


  clearStudentData = () => {
    this.selectedStudentData = {};
    console.log('clearStudentData', this.selectedStudentData);
  }

  
}
