import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule} from '@angular/forms' 


import { AppComponent } from './app.component';
import { ChildComponent } from './childComponent/child/child.component';
import { ParentComponent } from './parentComponent/parent/parent.component';
import { HighlightDirective } from './directives/highlight.directive';
import { PipesPipe } from './pipes.pipe';
import { DataService} from './service/api/data.service';

@NgModule({
  declarations: [
    AppComponent,
    ChildComponent,
    ParentComponent,
    HighlightDirective,
    PipesPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule ,
    FormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
