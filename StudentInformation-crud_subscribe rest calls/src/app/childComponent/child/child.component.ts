import { Component, OnInit, Input, Output ,EventEmitter} from '@angular/core';
import { DataService } from 'src/app/service/api/data.service';


@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent {
    styleObj: any = { backgroundColor : 'red' , color : 'white'};
    defaultStyleObj : any = { backgroundColor : '' , color : '' };

  @Input() student :any;
  @Output() selectedStudentData = new EventEmitter();
  @Output() deleteStudentData = new EventEmitter();
  // constructor(private dataservice: DataService) { 
  //   console.log("log")
  // }


  onSelectStudent = (student) => {
    console.log(student);
    this.selectedStudentData.emit(student);
  }


  onSelectDelete = (student) => {
    console.log(student);
    this.deleteStudentData.emit(student.id);
  }
  
}
