import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent implements OnInit {
  submitted : boolean=false;
  model: any = {
    userName:'',
    password: '',
    role: ''

  }
  
  constructor() { }  
  ngOnInit(): void {
  }

  submit = (loginRef) =>{   
    console.log(loginRef.valid); 
    this.submitted=true;    
    if(loginRef.valid)
    {
      console.log(this.model);
    }
  } 
}
