import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder ,FormArray} from '@angular/forms';
import { Key } from 'protractor';
/**
 * 
 * @param control @description User name length custom validation
 */
function userNameValidator(control: AbstractControl): { [key: string]: boolean } | null {
  if (control.value.length > 10) {
    return { 'maxlenghtError': true };
  }
  return null;
}

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {
  registerForm: FormGroup;
  submitted: boolean = false;
  validate = [Validators.required, Validators.minLength(5), userNameValidator]
   Data: Array<any> = [
    { name:'Telugu',value: 'Telugu' },
    { name:'English',value: 'English' },
    { name:'Hindhi',value: 'Hindhi' }
  ];
  constructor(private formBuilder: FormBuilder) {
    this.registerForm = this.formBuilder.group({
      checkArray: this.formBuilder.array([], [Validators.required])
    })
  }

  onCheckboxChange(e) {
    const checkArray: FormArray = this.registerForm.get('checkArray') as FormArray;
  
    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      FirstName: new FormControl('', this.validate),
      lastName: new FormControl('', this.validate),
      email: new FormControl('', this.validate),
      mobile: new FormControl('', this.validate),
      address: new FormControl('', this.validate),
      gender: new FormControl('', this.validate),
      role: new FormControl('', [Validators.required])
    });
  }

  submit = () => {
    this.submitted = true;
    if (this.registerForm.valid) {
      console.log(this.registerForm.value);
    }
  }
}


