import { Component, Input,Output,EventEmitter,OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

   @Input('student') studentObj :any;  
   @Output() selectedStudent=new EventEmitter();
   @Output() deleteStudent=new EventEmitter();

  constructor() {
    console.log("child initiated ");
   }

   onSelect =(studentObj:any) =>{
     console.log(studentObj);
    this.selectedStudent.emit(studentObj);   
    this.deleteStudent.emit(studentObj.id);
  }

  // onClearSelect=() => {
  //   this.clearSelectedStudent.emit();
  // }

  ngOnInit(): void {
    console.log(this.studentObj);
  } 

}
